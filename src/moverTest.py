import unittest
import os
import shutil
from mover import MediaMover, PathDetails

class TestMover(unittest.TestCase):

    cwd = os.getcwd()
    parent_dir = os.path.dirname(cwd)
    test_dir = os.path.join(parent_dir, 'tst')
    src_loc = os.path.join(test_dir, 'src_test') 
    clean_loc = os.path.join(test_dir, 'src_clean')
    target_loc = os.path.join(test_dir, 'target')

    test_image = os.path.join(src_loc, 'Kaitlyn.JPG')
    test_image_dup = os.path.join(src_loc, 'Kaitlyn2.JPG')
    test_image_mark = os.path.join(src_loc, 'KaitlynMark.JPG')
    test_image_mark2 = os.path.join(src_loc, 'KaitlynMark2.JPG')
    test_image_no_meta_data = os.path.join(src_loc, 'NoMetaData.jpg')
    
    def setUp(self):
        self.mover = MediaMover(self.src_loc, self.target_loc)
        if os.path.isdir(self.target_loc):
            shutil.rmtree(self.target_loc)

        if os.path.isdir(self.src_loc):
            shutil.rmtree(self.src_loc)

        shutil.copytree(self.clean_loc, self.src_loc)
        
        
    def test_PathDetails_parse(self):
        def assert_results(path_details):
            self.assertEqual(path_details.full_path, self.test_image)
            self.assertEqual(path_details.base_path, self.src_loc)
            self.assertEqual(path_details.file_name, 'Kaitlyn.JPG')
            self.assertEqual(path_details.base_file_name, 'Kaitlyn')
            self.assertEqual(path_details.extension, '.JPG')
        
        assert_results(PathDetails.create_by_qualified_path(self.test_image))
        assert_results(PathDetails.create_by_path_and_name(self.src_loc, 'Kaitlyn.JPG'))

    def test_file_counts(self):
        stats = self.mover.process_pictures()
        self.assertEqual(8, stats.proccessed_image_count)
        self.assertEqual(2, stats.dup_name)
        self.assertEqual(1, stats.exact_dup)
        self.assertEqual(2, stats.no_meta_data)
        self.assertEqual(6, stats.movie_count)
        self.assertEqual(2, stats.system_files)
        self.assertEqual(1, stats.other_count)

    def test_file_details(self):
        img_date = self.mover.image_details(self.test_image)
        self.assertEqual(2014, img_date.tm_year); 
        self.assertEqual(5, img_date.tm_mon); 
        self.assertEqual(18, img_date.tm_mday); 
        self.assertEqual(11, img_date.tm_hour); 
        self.assertEqual(7, img_date.tm_min); 
        self.assertEqual(1, img_date.tm_sec); 

    def test_image_path(self):
        self.mover.source_info = PathDetails.create_by_qualified_path(self.test_image)
        path = self.mover.build_img_path()
        self.assertEqual(os.path.join(self.target_loc, '2014','05', '05-18-2014 11.07.01.JPG'), path.full_path)
    
    def test_image_path_no_details(self):
        self.mover.source_info = PathDetails.create_by_qualified_path(self.test_image_no_meta_data)
        path = self.mover.build_img_path()
        self.assertEqual(path.full_path, os.path.join(self.target_loc, '0000-Unknown-Date', 'NoMetaData.jpg'))
        
    def test_duplicate_hash_found(self):
        target = os.path.join(self.target_loc, '2099', '10', 'image1.jpg')
        self.mover.copy_file(self.test_image, target)
        self.mover.source_info = PathDetails.create_by_qualified_path(self.test_image_dup)

        path = self.mover.check_unique(target, 'image1.jpg')
        self.assertEqual(path, os.path.join(self.target_loc, 'Duplicates', 'image1.jpg'))
    
    def test_same_file_name_incremented(self):
        #setup image in target 
        target_base = os.path.join(self.target_loc, '2099', '10')
        target = os.path.join(target_base, 'image.jpg')
        self.mover.copy_file(self.test_image, target)

        
        #First try should end in a _1
        #Move a similar image that will have a diff MD5
        self.mover.source_info = PathDetails.create_by_qualified_path(self.test_image_mark)
        path = self.mover.check_unique(target, 'image1.jpg') #Target should be found and new path returned
        self.assertEqual(path, os.path.join(target_base, 'image_1.jpg'))
        
        #setup second test
        target_2 = path
        self.mover.copy_file(self.test_image_mark, target_2)
        
        #Send dup should end in a _2
        self.mover.source_info = PathDetails.create_by_qualified_path(self.test_image_mark2)
        path = self.mover.check_unique(target, 'image1.jpg')
        self.assertEqual(path, os.path.join(target_base, 'image_2.jpg'))

    def test_copy_file(self):
        target = os.path.join(self.target_loc, '2099', '10', 'image1.jpg')
        self.mover.copy_file(self.test_image, target)
        self.assertFalse(os.path.isfile(self.test_image), 'Image should be removed from source')
        self.assertTrue(os.path.isfile(target), 'File should be in now location')

if __name__ == '__main__':
    unittest.main()
