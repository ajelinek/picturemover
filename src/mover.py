import os
import exifread
import time
import shutil
import hashlib
import sys

def md5_for_file(in_file):
    md5 = hashlib.md5()
    while True:
        data = in_file.read()
        if not data:
            break
        md5.update(data)
    
    return md5.digest() 


class MediaMover(object):
    
    def __init__(self, base_src, base_target):
        self.base_src = base_src
        self.base_target = base_target
        self.stats = Stats()
        self.source_info = None
        self.target_info = None

    def process_pictures(self):
        for path, dirs, files in os.walk(self.base_src):  # @UnusedVariable
            self.process_files(path, files)

        return self.stats

    def image_details(self, file_path):
        img = open(file_path, 'rb')
        tags = exifread.process_file(img, details=False)
        img.close()

        def processDate(string_date):
            date = None
            try:
                date = time.strptime(string_date, '%Y:%m:%d %H:%M:%S')
            except:
                try:
                    date = time.strptime(string_date, '%m.%d.%Y %H:%M:%S')
                except:
                    print('Error processing date for file: ' + file_path)
                    print(
                        'Date Returned: ' + string_date)
                    #raise
            return date

        if 'EXIF DateTimeOriginal' in tags:
            img_date = processDate(str(tags['EXIF DateTimeOriginal']))
            return img_date

        if 'EXIF CreateDate' in tags:
            img_date = processDate(str(tags['EXIF CreateDate']))
            return img_date

        if 'EXIF ModifyDate' in tags:
            img_date = processDate(str(tags['EXIF ModifyDate']))
            return img_date
        

    def process_files(self, path, files):
        for file_name in files:
            self.skip = False
            self.source_info = PathDetails.create_by_path_and_name(path, file_name)
            
            ext = self.source_info.extension.upper()
            source_full_path = self.source_info.full_path

            if ext in ('.JPG', '.JPEG', '.GIF'):
                self.stats.proccessed_image_count += 1
                self.target_info = self.build_img_path()
                self.copy_file(source_full_path, self.target_info.full_path)

            elif ext == '.PNG':
                self.stats.screen_shot_count += 1
                self.copy_file(source_full_path, self.base_target + '/Screen-Shots/' + file_name)

            elif ext in ('.INI', '.DB', '.INFO'):
                self.stats.system_files += 1

            elif ext in ('.THM', '.AVI', '.MOV', '.3GP', '.MP4', '.MPG'):
                self.stats.movie_count += 1
                self.copy_file(source_full_path, self.base_target + '/movies/' + file_name)

            else:
                self.stats.other_count += 1
                self.copy_file(source_full_path, self.base_target + '/unknown-files/' + file_name)

    def build_img_path(self):
        """Build the target paths for images based on image details. This will include the path and name"""

        details = self.image_details(self.source_info.full_path)
        
        if not details:
            self.stats.no_meta_data += 1
            return PathDetails.create_by_qualified_path(os.path.join(self.base_target,  '0000-Unknown-Date', self.source_info.file_name))

        def convert_and_padd(num):
            return str(num).zfill(2)

        year = str(details.tm_year)
        month = convert_and_padd(details.tm_mon)
        
        file_name = month + '-' + convert_and_padd(details.tm_mday) + '-' + year + ' '
        file_name = file_name + convert_and_padd(details.tm_hour) + '.'
        file_name = file_name + convert_and_padd(details.tm_min) + '.'
        file_name = file_name + convert_and_padd(details.tm_sec)
        file_name = file_name + self.source_info.extension
        
        path = os.path.join(self.base_target, year, file_name)
        
        return PathDetails.create_by_qualified_path(self.check_unique(path, file_name))
    
    def check_unique(self, path, file_name):
        unique_tic = 0
        wrk_path = path

        if os.path.isfile(wrk_path): 
            src_file = open(self.source_info.full_path, 'rb')
            trg_file = open(wrk_path, 'rb')
            source_hash = md5_for_file(src_file) 
            target_hash = md5_for_file(trg_file)

            src_file.close()
            trg_file.close()
            if source_hash == target_hash:
                self.stats.exact_dup += 1
                wrk_path = os.path.join(self.base_target, 'Duplicates', file_name)
            else:
                self.stats.dup_name +=1

        #Create a unique Name
        while os.path.isfile(wrk_path):
            unique_tic += 1
            wrk_target = PathDetails.create_by_qualified_path(path)
            wrk_path = os.path.join(wrk_target.base_path, wrk_target.base_file_name + '_' + str(unique_tic) + wrk_target.extension)
        
        return wrk_path


    def copy_file(self, source_path, target_path):

        def check_dir(dir_path):
            if not os.path.isdir(dir_path):
                os.makedirs(dir_path)

        check_dir(os.path.split(target_path)[0])

        print('Processed Images: ' + str(self.stats.proccessed_image_count) + '  target: ' + target_path + '  Source: ' + source_path)
        shutil.move(source_path, target_path)
        self.stats.moved_files += 1
        #shutil.copy2(source, l_target)
   
    
class Stats(object):
    def __init__(self):
        self.proccessed_image_count = 0
        self.moved_files = 0
        self.system_files = 0
        self.screen_shot_count = 0
        self.movie_count = 0
        self.other_count = 0
        self.exact_dup = 0
        self.dup_name = 0
        self.no_meta_data = 0

class PathDetails(object):
    def __init__(self):
        self.full_path = ''
        self.base_path = ''
        self.file_name = ''
        self.base_file_name = ''
        self.extension = ''
        
    def __parse_details__(self, full_path):
        self.full_path = full_path

        temp = os.path.split(self.full_path)
        self.base_path = temp[0] 
        self.file_name = temp[1]

        temp = os.path.splitext(self.file_name)
        self.base_file_name = temp[0]
        self.extension = temp[1]
    
    @classmethod
    def create_by_qualified_path(cls, full_path):
        details = PathDetails()
        details.__parse_details__(full_path)
        return details
        
    @classmethod
    def create_by_path_and_name(cls, path, file_name):
        details = PathDetails()
        details.__parse_details__(os.path.join(path, file_name))
        return details
        
        

if __name__ == '__main__':
    source_dir = sys.argv[1]
    target_dir = sys.argv[2]
    if os.path.isdir(source_dir):
        print (MediaMover(source_dir, target_dir).process_pictures().__dict__)
    else:
        print ('Not a Valid Source or Dir')
